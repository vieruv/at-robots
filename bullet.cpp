/*
 *File: bullet.cpp
 *Author: Skye O'Halloran
 *NOTE: still under construction, everything should compile
 *but I haven't really done any tests yet
 */

#include "bullet.h"
#include <math.h>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
//#include "Robot.h"
//#include "Robot.cpp"


//Reminders of constants from "bullet.h"
//const float PI = 3.1415926535;
//const int arenaX = 0;
//const int arenaY = 0;
//const int arenaW = 640;
//const int arenaH = 480;
//Note: the actual arena variables are pending an
//update from Vlad


//Constructor and Destructor deffinition
Bullet::Bullet(float x, float y, float degree) {
	if (check_degree(degree) >= 0) {
		valid = true;
		x1 = x;
		y1 = y;
		direction = degree;
		length = 5;
		//setting x2
		int adj = length * cos(deg_to_rad(direction));
		if (direction < 90 && direction >= 270) {
			x2 = x1-adj;
		} else {
			x2 = x1+adj;
		}
		//setting y2
		int opp = length * sin(deg_to_rad(direction));
		if (direction < 180) {
			y2 = y1-opp;
		} else {
			y2 = y1+opp;
		}
		//setting 'constants'
		width = 3;
		damage = 1;
		speed = 5;
		R = 255;
		G = 0;
		B = 0;
		A = 100;
	} else {
		//else everything is initialized to whatever its
		//default value is / NULL
		//therefore, the 'valid' flag is used to check for an
		//a useable bullet
		valid = false;
	}
};

Bullet::~Bullet(){}

//member function definitons
//-----------------------------------------------------------

//Degree related functions
//-----------------------------------------------------------
/**
	checks to see if the direction is valid (0<= x <360)
	@param: the degree to check
	@return: either the original degree, the original%360, or
	-1 if original was negative
*/
float  Bullet::check_degree(float degree) {
	if (degree >= 0 && degree < 360) {
		return degree;
	}
	if (degree < 0) {
		return -1;
	}
	float newDegree = fmod(degree, 360);
	return newDegree;
}

/**
	converts a degree to radians
	@param: a float degree
	@return: a float radian
*/
float Bullet::deg_to_rad(float degree) {
	float good_degree = check_degree(degree);
	if (good_degree > -1) {
		float a = good_degree * PI;
		float b = a/180;
		return b;
	}
	return -1;
}


//not in use
/**
	converts a radian to degrees
	@param: a float radian
	@return: a float degree
*/
float Bullet::rad_to_deg(float radian) {
	if (radian >= 0 && radian <= 2*PI) {
		float a = radian*180;
		float b = a/PI;
		return b;
	}
	return -1;
}

/**
	determined the guadrant (1,2,3,4) on the coordinate plane that
	the bullet thickLine is in, relative to the bullet point
	(x1,y1) being equal to (0,0)
	if the bullet direction is 0,360 it is assigned 5
	if the bullet direction 90 it is assigned 6
	if the bullet direction is 180 it is assigned 7
	if the bullet direction is 270 it is assigned 8
	@param: the float degree of the line
	@return int 1,2,3,4,5,6,7,8  or -1
*/
int Bullet::getQuadrant(float degree) {
	float deg = check_degree(degree);
	if (deg == 0 || deg == 360) {
		return 5;
	}
	if (deg == 90) {
		return 6;
	}
	if (deg == 180) {
		return 7;
	}
	if (deg == 270) {
		return 8;
	}
	if (deg > 0 && deg < 90) {
		return 1;
	}
	if (deg > 90 && deg <180) {
		return 2;
	}
	if (deg > 180 && deg <270) {
		return 3;
	}
	if (deg > 270 && deg <360) {
		return 4;
	}
	return -1;
}

//Getter functions
//----------------------------------------------------------------
bool Bullet::is_valid() {
	return valid;
}
float Bullet::getX1() {
	return x1;
}
float Bullet::getY1() {
	return y1;
}
float Bullet::getX2() {
	return x2;
}
float Bullet::getY2() {
	return y2;
}
float Bullet::getDirection() {
	return direction;
}
int Bullet::getR() {
	return R;
}
int Bullet::getG() {
	return G;
}
int Bullet::getB() {
	return B;
}
int Bullet::getA() {
	return A;
}
int Bullet::getSpeed() {
	return speed;
}
int Bullet::getDamage() {
	return damage;
}
int Bullet::getLength() {
	return length;
}
int Bullet::getWidth() {
	return width;
}

//Setter functions
//-------------------------------------------------------------
void Bullet::set_valid(bool v) {
	valid = v;
}
void Bullet::setX1(float x) {
	x1 = x;
}
void Bullet::setY1(float y) {
	y1 = y;
}
void Bullet::setX2(float x) {
	x2 = x;
}
void Bullet::setY2(float y) {
	y2 = y;
}

//Collision detection
//------------------------------------------------------------
/**
	determines if the bullet has collided with the edge of
	the screen--Note: the arena rectangle is conceptualy
	defined by rect(x,y,width, height) and the arena rect
	is rect(x!=0, y!=0, w, h)
	@param: the height and width of the screen
	@return: bool true if the bullet hits the edges
*/
bool Bullet::collide_wall(int aX, int aY, int aW, int aH) {
	float x_1 = getX1();
	float y_1 = getY1();
	if (x_1 <= aX || x_1 >= aW) {
		return true;
	}
	if (y_1 <= aY || y_1 >= aH) {
		return false;
	}
	return false;
}

/**
	determines if the bullet has collided with any robot in
	the arena, by looping through an std::vector of the robots
	Note: the bullet collides with a robot if the bullet(x2,y2)
	is within a radius of 15 pixels from the robot(x,y) center
	@param: a std::vector<> of Robot objects
	@return: bool true if the bullet hit a robot
*/
bool Bullet::collide_robot(const std::vector<Robot> & robots){
	double distance;
	double range = 15;
	for (auto & r : robots) {
		distance = get_distance(r);
		if (distance <= range) {
			//r.isHit = true;
			return true;
		}
	}
	return false;
}

/**
	gets the distance between the leading point of the bullet(x2,y2)
	and the center of the robot
	@param: a Robot object
	@return: the float distnce between the two, or -1 if the robot
	object is null
*/
float Bullet::get_distance(Robot robot) {
	/*
	if (robot == NULL) {
		return -1;
	}
	*/
	double distance;
	double bX = (double)getX2();
	double bY = (double)getY2();
	double rX = (double)robot.x;
	double rY = (double)robot.y;
	double xDif = rX - bX;
	double yDif = rY - bY;
	double xDifsq = xDif * xDif;
	double yDifsq = yDif * yDif;
	distance = sqrt(xDifsq+yDifsq);

	return distance;
}

//Motion functions
//-------------------------------------------------------------
/**
	finds the next x1 position based on direction
	@param: void
	@return: the float position of x1 next
*/
float Bullet::nextX1() {
	int quad = getQuadrant(direction);
	float x = getX1();
	switch (quad) {
		case 1:
			return x+1;
			break;
		case 2:
			return x-1;
			break;
		case 3:
		return x-1;
			break;
		case 4:
		return x+1;
			break;
		case 5:
		return x+1;
			break;
		case 6:
		return x;
			break;
		case 7:
		return x-1;
			break;
		case 8:
		return x;
			break;
		default:
			return x;
	}
}

/**
	finds the next y1 position based on direction
	@param: void
	@return: the float position of y1 next
*/
float Bullet::nextY1() {
	int quad = getQuadrant(direction);
	float y = getY1();
	switch (quad) {
		case 1:
			return y-1;
			break;
		case 2:
			return y-1;
			break;
		case 3:
		return y+1;
			break;
		case 4:
		return y+1;
			break;
		case 5:
		return y;
			break;
		case 6:
		return y-1;
			break;
		case 7:
		return y;
			break;
		case 8:
		return y+1;
			break;
		default:
			return y;
	}
}

/**
	finds the next x2 position based on direction
	@param: void
	@return: the float position of x2 next
*/
float Bullet::nextX2() {
	int quad = getQuadrant(direction);
	float x = getX2();
	switch (quad) {
		case 1:
			return x+1;
			break;
		case 2:
			return x-1;
			break;
		case 3:
		return x-1;
			break;
		case 4:
		return x+1;
			break;
		case 5:
		return x+1;
			break;
		case 6:
		return x;
			break;
		case 7:
		return x-1;
			break;
		case 8:
		return x;
			break;
		default:
			return x;
	}
}

/**
	finds the next y2 position based on direction
	@param: void
	@return: the float position of y2 next
*/
float Bullet::nextY2() {
	int quad = getQuadrant(direction);
	float y = getY2();
	switch (quad) {
		case 1:
			return y-1;
			break;
		case 2:
			return y-1;
			break;
		case 3:
		return y+1;
			break;
		case 4:
		return y+1;
			break;
		case 5:
		return y;
			break;
		case 6:
		return y-1;
			break;
		case 7:
		return y;
			break;
		case 8:
		return y+1;
			break;
		default:
			return y;
	}
}

/**
	updates the coordinates of the bullet (x1,y1) and (x2,y2)
	@param: none
	@return: none
*/
void Bullet::move() {
	float nX1 = nextX1();
	float nY1 = nextY1();
	float nX2 = nextX2();
	float nY2 = nextY2();
	x1 = nX1;
	y1 = nY1;
	x2 = nX2;
	y2 = nY2;
}

/**
	keeps track of whether the bullet should continue moving,
	or stop moving if it has collided with something
	@param: int dimensions of the arena, and a vector  of robots
	@return: bool true if no collisions, false if some Collision
*/
bool Bullet::keep_moving(int aX, int aY, int aW, int aH, const std::vector<Robot> & robots) {
	if (collide_wall(aX, aY, aW, aH)) {
		return false;
	}
	if (collide_robot(robots)) {
		return false;
	}
	return true;
}

/**
	renders bullet in its current position
	*Coordinates of Bullet: note, the point (x1, y1)
	*reffers to the point of origination of the bullet,
	*the center of the robot---- the point (x2, y2) reffers
	*to the point that extends the bullet length, outward
	*from the center of the robots
	*therefore, for rendering with, the call should
	* actually be thickLineRGBA(renderer, bullet.x2, bullet.y2,
	*bullet.x1, bullet.y1, R, G, B, A)
	@param: the renderer object
	@return: void
*/
void Bullet::renderBullet(SDL_Renderer* renderer) {
	thickLineRGBA(renderer, getX2(), getY2(), getX1(), getY1(), getWidth(), getR(), getG(), getB(), getA());
}

/**
	gets rid of a bullet when it collides with something
	by setting its 'valid' flag to false
	@param: none
	@return: none
*/
void Bullet::despawn() {
	set_valid(false);
}
