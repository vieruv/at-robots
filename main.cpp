/* 
 * File:   main.cpp
 * Author: AT_Robots Team
 *
 * Created on February 23, 2017, 5:15 PM
 */

#include <cstdlib>
#include <iostream>
#include <SDL2/SDL.h>
#include <random>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_config.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <stdint.h>
#include <inttypes.h>

#include "miniat.h"
#include "peripherals.h"
#include "ports.h"
#include "robot.h"
#include "gun.h"
#include "bullet.h"

using namespace std;
int mainStart (int argc, char *argv[]);

/*
 * 
 */
Robot* rArray[2] = {};
int size = 2;
std::vector<Bullet*> bullets;
void collide_robot();

void drawText(SDL_Surface*screen, char* strin1, int size, int x, int y, int fR, int fG, int fB, int bR, int bG, int bB)
{
    TTF_Font* font = TTF_OpenFont("batmfa.ttf",size);
    SDL_Color foregroundColor = {fR, fG, fB};
    SDL_Color backgroundColor = {bR, bG, bB};
    SDL_Surface* textSurface = TTF_RenderText_Shaded(font, strin1, foregroundColor, backgroundColor);
    SDL_Rect textLocation = {x, y, 0, 0};
    //perform a fast surface copy to a destination surface
    SDL_BlitSurface(textSurface, NULL, screen, &textLocation);
    //frees an RGB surface
    SDL_FreeSurface(textSurface);
    TTF_CloseFont(font);
}

int main(int argc, char *argv[]){
	TRY {
		mainStart(argc, argv);
	}
		CATCHALL {
		miniat_dump_error();
	}
	FINALLY {}
	ETRY;

	return EXIT_SUCCESS;
}

int mainStart(int argc, char *argv[]) {
    SDL_Window* window = NULL;
    /*
        SDL_Surface* surfaceMessage1 = NULL;
	SDL_Surface* surfaceMessage2 = NULL;
	SDL_Surface* surfaceMessage3 = NULL;
	SDL_Surface* surfaceMessage4 = NULL;
    */
    SDL_Surface* surface = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_Init(SDL_INIT_VIDEO);
/*
    if(TTF_Init()<0){
        std::cout << "Error" << TTF_GetError() << std::endl;
	}
*/
    window = SDL_CreateWindow("AT Robots", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN);
    surface = SDL_GetWindowSurface(window);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
    /*std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<int> x(1, 300);
    std::uniform_int_distribution<int> y(1, 200);
    std::uniform_int_distribution<int> angle(1, 360);
    std::uniform_int_distribution<int> color(0x00, 0xFF);
*/
	
    
    //creates the array of robots and the robots
    int i = 0;
    miniat *m = NULL;
    FILE *rName = NULL;

    for (i = 0; i < size; i++){
	rName = fopen(argv[i+1], "r+b");
	/*if(!rName) {
		fprintf(stderr, "Couldn't open \"%s\".  %s", input_filename, strerror(errno));
		exit(EXIT_FAILURE);*/
	m = miniat_new(rName, NULL);
        rArray[i] = new Robot (100 + 200*i, 100, 0, 0x00, 0x00, 0xff, m);
        //rArray[i]->addWeapon();
        rArray[i]->renderRobot (renderer);
	rArray[i] -> shoot();
    }
    /*
        TTF_Font* Sans = TTF_OpenFont("batmfa.ttf",11);
        SDL_Color White = {255, 255, 255};
        surfaceMessage1 = TTF_RenderText_Solid(Sans, "Robot1", White);
        surfaceMessage2 = TTF_RenderText_Solid(Sans, "Robot2", White);
        surfaceMessage3 = TTF_RenderText_Solid(Sans, "Robot3", White);
        surfaceMessage4 = TTF_RenderText_Solid(Sans, "Robot4", White);
        
	SDL_Texture* Message1 = SDL_CreateTextureFromSurface(renderer, surfaceMessage1);
        SDL_Texture* Message2 = SDL_CreateTextureFromSurface(renderer, surfaceMessage2);
        SDL_Texture* Message3 = SDL_CreateTextureFromSurface(renderer, surfaceMessage3);
        SDL_Texture* Message4 = SDL_CreateTextureFromSurface(renderer, surfaceMessage4);
        SDL_Rect Message1_rect;
        SDL_Rect Message2_rect;
        SDL_Rect Message3_rect;
        SDL_Rect Message4_rect;
        if(Sans == nullptr){
            std::cout << "Failed to load font: " << SDL_GetError() << std::endl;
            return false;
        }
        Message1_rect.x = 550;
        Message1_rect.y = 50;
        Message1_rect.w = 50;
        Message1_rect.h = 25;

        Message2_rect.x = 550;
        Message2_rect.y = 115;
        Message2_rect.w = 50;
        Message2_rect.h = 25;

        Message3_rect.x = 550;
        Message3_rect.y = 175;
        Message3_rect.w = 50;
        Message3_rect.h = 25;

        Message4_rect.x = 550;
        Message4_rect.y = 240;
        Message4_rect.w = 50;
        Message4_rect.h = 25;
      */  
	
    //forever
    while(true) {
        
        SDL_Event e;
        if(SDL_PollEvent(&e)) {
            if(e.type == SDL_QUIT || e.key.keysym.sym == SDLK_q) {
                break;
            }
        }
        //runs checks each cycle
        for (int c = 0; c < 20; c++){
            for (i = 0; i < size; i++){
		miniat_clock(rArray[i] -> m);
		peripherals_clock(rArray[i]);
		//ports_clock(m);
                rArray[i]->checks ();
		collide_robot();
            }
        }
        //updates the robot position and renders each frame
        
        SDL_RenderClear(renderer);
        
	boxRGBA(renderer, 640, 0, 0, 480, 0xae, 0xae, 0xae, 0xff);
        

	
/*
	SDL_SetRenderDrawColor(renderer, 191, 191, 191, SDL_ALPHA_OPAQUE);
        SDL_RenderCopy(renderer, Message1, NULL, &Message1_rect);
	SDL_RenderCopy(renderer, Message2, NULL, &Message2_rect);
	SDL_RenderCopy(renderer, Message3, NULL, &Message3_rect);
	SDL_RenderCopy(renderer, Message4, NULL, &Message4_rect);
    */
        
        
        for (i = 0; i < size; i++){
	  //SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0xff, SDL_ALPHA_OPAQUE);
		if(rArray[i] -> getHealth() > 0)
            		rArray[i]->renderRobot (renderer);
        }
        boxRGBA(renderer, 15, 0, 0, 480, 0x01, 0x00, 0x00, 0xff);
	boxRGBA(renderer, 640, 0, 0, 15, 0x01, 0x00, 0x00, 0xff);
	boxRGBA(renderer, 640, 465, 0, 480, 0x00, 0x00, 0x00, 0xff);
	boxRGBA(renderer, 640, 0, 540, 480, 0x00, 0x00, 0x00, 0xff);

	//Color
	boxRGBA(renderer, 555, 23, 600, 43, 0xff, 0x00, 0x00, 0xff);
	//Health box
	boxRGBA(renderer, 635, 43, 610, 23, 0x00, 0xff, 0x00, 0xff);

	//Color
	boxRGBA(renderer, 555, 80, 600, 100, 0x00, 0xff, 0x00, 0xff);
	//Health box
	boxRGBA(renderer, 635, 100, 610, 80, 0x00, 0xff, 0x00, 0xff);

	//Color
	boxRGBA(renderer, 555, 145, 600, 165, 0x00, 0x00, 0xff, 0xff);
	//Health box
	boxRGBA(renderer, 635, 166, 610, 144, 0x00, 0xff, 0x00, 0xff);

	//Color
	boxRGBA(renderer, 555, 210, 600, 230, 0xff, 0xff, 0x00, 0xff);
        //Health box
        boxRGBA(renderer, 635, 230, 610, 210, 0x00, 0xff, 0x00, 0xff);

	SDL_RenderDrawLine(renderer, 640, 16, 540, 16);
        SDL_RenderDrawLine(renderer, 640, 70, 540, 70);
        SDL_RenderDrawLine(renderer, 640, 135, 540, 135);
        SDL_RenderDrawLine(renderer, 640, 198, 540, 198);
        SDL_RenderDrawLine(renderer, 640, 263, 540, 263);
        //???????
        //SDL_SetRenderDrawColor(renderer, 191, 191, 191, SDL_ALPHA_OPAQUE);
        //SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);
	SDL_Delay(33);
        
        
    }
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();

    return 0;

}

double get_distance(Robot* robot, Bullet* bullet) {\
    //gets the distance between the robot's center point and the leading point of the bullet
	/*
	if (robot == NULL) {
		return -1;
	}
	*/
	double distance;
	double bX = (double)bullet -> getX();
	double bY = (double)bullet -> getY();
	double rX = (double)robot -> getX();
	double rY = (double)robot -> getY();
	double xDif = rX - bX;
	double yDif = rY - bY;
	double xDifsq = xDif * xDif;
	double yDifsq = yDif * yDif;
	distance = sqrt(xDifsq+yDifsq);

	return distance;
}

void collide_robot(){
	double distance;
	double range = 15;
	for (int i = 0; i < size; i++) {
		for(int j=0; j<size; j++){
            		for (int k = 0; k < rArray[j] -> bullets.size(); j++){
                		distance = get_distance(rArray[i], rArray[j] -> bullets.at(k));
				printf("%f", distance);
				if (distance <= range) {
					rArray[i] -> setHealth(25);
                        		rArray[j] -> bullets.erase(rArray[j] -> bullets.begin() + k);
				}
               	 	}
            	}
        }
}