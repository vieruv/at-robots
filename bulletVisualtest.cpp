/*
 *File: bulletVisualtest.cpp
 *Author: Skye O'Halloran
 */

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include "bullet.h"
#include "bullet.cpp"
#include "Robot.h"
//#include "Robot.cpp"
#include <vector>
#include <iostream>
#include <math.h>
using namespace std;

//Screen dimension constants
const int S_WIDTH = 700;
const int S_HEIGHT = 500;

//window, surface, and renderer
SDL_Window* bWindow = NULL;
SDL_Surface* surf = NULL;
SDL_Renderer* bRender = NULL;

//Initialization of window
bool init() {
	bool success = true; //flag
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		success = false;
		cout << "SDL failed to initialize.\n";
	} else {
		bWindow = SDL_CreateWindow("bulletVisualtest.cpp", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, S_WIDTH, S_HEIGHT, SDL_WINDOW_SHOWN); //creates window
		if (bWindow == NULL) {
			success = false;
			cout << "Window failed to initialize.\n";
		} else {
			surf = SDL_GetWindowSurface(bWindow);
			//create renderer
			bRender = SDL_CreateRenderer(bWindow, -1, SDL_RENDERER_ACCELERATED);
			if (bRender == NULL) {
				success = false;
				cout << "Failed to initialize renderer.\n";
			} else {
				SDL_SetRenderDrawColor(bRender, 0, 0, 0, 0);
			}
		}
	}
	return success;
}

//Closes window
void close() {
	SDL_DestroyRenderer(bRender);
	SDL_DestroyWindow(bWindow);
	bRender = NULL;
	bWindow = NULL;
	SDL_Quit();
}


//render a mock robot
void renderMockRob(SDL_Renderer* renderer, float x, float y) {
  filledCircleRGBA(renderer, x, y, 15, 20, 200, 50, 255);
}

int main (int argc, char* args[]) {

	//just outputs the name for reference
	cout << "bulletVisualtest.cpp\n";

  //makes a vector of robots and a vector of bullets
  vector<Bullet> bullets;
  bullets.push_back(Bullet(320,280,0));
  bullets.push_back(Bullet(320,280,30));
  bullets.push_back(Bullet(320,280,45));
  bullets.push_back(Bullet(320,280,60));
  bullets.push_back(Bullet(320,280,90));
  bullets.push_back(Bullet(320,280,120));
  bullets.push_back(Bullet(320,280, 135));
  bullets.push_back(Bullet(320,280,150));
  bullets.push_back(Bullet(320,280,180));
  bullets.push_back(Bullet(320,280,210));
  bullets.push_back(Bullet(320,280,225));
  bullets.push_back(Bullet(320,280,240));
  bullets.push_back(Bullet(320,280,270));
  bullets.push_back(Bullet(320,280,300));
  bullets.push_back(Bullet(320,280,315));
  bullets.push_back(Bullet(320,280,330));
  cout << "\n bullets in use: \n";
  for (auto & b : bullets) {
    cout << "(" << b.getX1() << ", " << b.getY1() << ", " <<
      b.getDirection() << ") " << b.is_valid() << "\n";
  }

  vector<Robot> robots;
  Robot a; a.x = 20; a.y = 280;
  Robot b; b.x = 620; b.y = 280;
  Robot c; c.x = 320; c.y = 80;
  Robot d; d.x = 320; d.y = 480;
  robots.push_back(a);
  robots.push_back(b);
  robots.push_back(c);
  robots.push_back(d);
  cout << "\n robots in use: \n";
  for (auto & r : robots) {
    cout << "(" << r.x << ", " << r.y << ")\n";
  }


	//start SDL and create window
	if (!init() ) {
		cout << "Failed to initialize.\n";
	} else {
		// do something
		bool quit = false; //main loop flag
		SDL_Event e; //event handler

		//main loop
		while (!quit) {
			while (SDL_PollEvent(&e) != 0) {
				if (e.type == SDL_QUIT) {
					quit = true;
				}
			}
			//clear screen
			SDL_SetRenderDrawColor(bRender, 100, 100, 100, 255);
			SDL_RenderClear(bRender);

			//render whatever
      for (auto & r : robots) {
        renderMockRob(bRender, r.x, r.y);
      }

      for (auto & p : bullets) {
        if (p.is_valid()) {
          p.renderBullet(bRender);
          if (p.collide_wall(arenaX, arenaY, arenaW, arenaH)) {
            p.despawn();
          } else if (p.collide_robot(robots)) {
            p.despawn();
          } else {
            p.move();
          }
        }
      }

/*
      for (int i = 0; i < 30; i++) {
        for (auto & b : bullets) {
          if (b.is_valid()) {
            b.renderBullet(bRender);
            b.move();
          }
        }
      }
*/

      //get it on the screen
      SDL_RenderPresent(bRender);
      SDL_Delay(300);


		}
	}

	close();
	return 0;
}