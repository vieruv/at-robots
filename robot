#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>

#define PI 3.14159265

int main(int argc, char* args[]){
	SDL_Window* wind = NULL;
	SDL_Surface* surf = NULL;
	SDL_Renderer* rend = NULL;
	int deg = 0;
	int deg2 = 90;
	int flag = 0;
    
	SDL_Init(SDL_INIT_VIDEO);
    
	wind = SDL_CreateWindow("Hopefully this looks like a robot", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN);
	surf = SDL_GetWindowSurface(wind);
	rend = SDL_CreateRenderer(wind, -1, SDL_RENDERER_SOFTWARE);

	while(true){
		SDL_Event e;
		if(SDL_PollEvent(&e)){
			if(e.type == SDL_QUIT){
				break;
			}
		}
		SDL_RenderClear(rend);
        
		boxRGBA(rend, 640, 0, 0, 480, 0xa0, 0xa0, 0xa0, 0xff);
		filledTrigonRGBA(rend, 320, 220, 310, 260, 330, 260, 0xff, 0x00, 0x00, 0xff);
		filledTrigonRGBA(rend, 320, 245, 320-200*cos(deg2*PI/180)+80*cos(((deg2+90)%360)*PI/180), 245-200*sin(deg2*PI/180)+80*sin(((deg2+90)%360)*PI/180), 320-200*cos(deg2*PI/180)-80*cos(((deg2+90)%360)*PI/180), 245-200*sin(deg2*PI/180)-80*sin(((deg2+90)%360)*PI/180), 0xff, 0xff, 0xff, 0x80);
		thickLineRGBA(rend, 320, 245, 320+25*cos(deg*PI/180), 245+25*sin(deg*PI/180), 4, 0x00, 0x00, 0x00, 0xff);
        
		SDL_RenderPresent(rend);
        
		deg++;
		deg2--;
		flag++;
		if(flag == 3){
			deg2--;
			flag = 0;
		}
		if(deg == 360)
			deg = 0;
		if(deg2 == 0)
			deg2 = 360;
		SDL_Delay(10);
	}
	SDL_DestroyWindow(wind);
	SDL_DestroyRenderer(rend);
	SDL_Quit();
	return 0;
}