/* 
 * File:   Robot.cpp
 * Author: grecoms
 * 
 * Created on February 23, 2017, 5:17 PM
 */

#include "Robot.h"
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#define PI 3.14159265358979323846
#define OFFSETTHROTTLE 1

Robot::Robot() {
    //addWeapon();
    //addScanner();
}

void Robot::renderRobot (SDL_Renderer* renderer, Robot r, float x, float y, float angle, int colorR, int colorG, int colorB) {
    //renders the robot at current x, y, and angle with specific colors
    r.x = x;
    r.y = y;
    r.currentAngle = angle;
    r.colorR = colorR;
    r.colorG = colorG;
    r.colorB = colorB;
    //renders red filled triangle
    filledTrigonRGBA(renderer, x+20*cos(angle*PI/180), y-20*sin(angle*PI/180), x-20*cos(angle*PI/180)-10*cos((angle+90)*PI/180), y+20*sin(angle*PI/180)+10*sin((angle+90)*PI/180), x-20*cos(angle*PI/180)+10*cos((angle+90)*PI/180), y+20*sin(angle*PI/180)-10*sin((angle+90)*PI/180), colorR, colorG, colorB, 0xff);
}

/*void Robot::addWeapon (Weapon w) {
    //calls weapon class to instantiate an object on this robot
}

void Robot::addScanner (Scanner s) {
    //calls scanner class to instantiate an object on this robot
}*/

void Robot::setHealth (Robot r, int h) {
    //sets health value
    r.health = health - h;
}

void Robot::setTurnAngle (Robot r, float angle) {
    //sets target turn angle value + counterclockwise - clockwise
    r.targetAngle = angle;
}

void Robot::setOffsetAngle (Robot r, float angleChange) {
    //sets offset turn angle value + counterclockwise - clockwise
    r.offsetAngle = angleChange;
}

void Robot::setThrottle (Robot r, float target) {
    //sets target speed value -75 to 100
    r.targetSpeed = target;
}

float turn (float current, float target, float offset) {
    //turns the robot clockwise or counterclockwise
    if (abs (target - current) > offset) {
        if (current < target){
            current += offset;
        }
        else{
            current -= offset;
        }
    }
    else{
        current = target;
    }
    return current;
}

float drive (float current, float target) {
    //drives the robot forward or backward
        if (abs (target - current) > OFFSETTHROTTLE) {
        if (current < target){
            current += OFFSETTHROTTLE;
        }
        else{
            current -= OFFSETTHROTTLE;
        }
    }
    else{
        current = target;
    }
    return current;
}

void Robot::checks (Robot r) {
    while (true) {
        if (collideRobot) {
            //stop the robot from moving
        }
        if (isHit) {
            //take damage
            //WHAT IF HIT BY 2 BULLETS AT SAME TIME?!?!?!?
        }
        if (health == 0) {
            //blow up the robot
        }
        //weapon.tick();
    }
    while (r.currentAngle != r.targetAngle) {
        turn (r.currentAngle, r.targetAngle, r.offsetAngle);
    }
    while (r.currentSpeed != r.targetSpeed) {
        drive (r.currentSpeed, r.targetSpeed);
    }
}

Robot::~Robot() {
    
}
