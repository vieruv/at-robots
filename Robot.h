/* 
 * File:   Robot.h
 * Author: grecoms
 *
 * Created on February 23, 2017, 5:17 PM
 */

#ifndef ROBOT_H
#define	ROBOT_H
#include <string>
#include <SDL2/SDL.h>

class Robot {
    private:
    int health = 100;
    float targetAngle;
    float offsetAngle;
    float currentSpeed;
    float targetSpeed;
    float turnSpeed;
    int colorR;
    int colorG;
    int colorB;
    std::string name;
    bool collideRobot;
    //Weapon weapon;
    //Scanner scanner;
  
    public:
    Robot ();
    void renderRobot (SDL_Renderer*, Robot, float, float, float, int, int, int);
    //void addWeapon (Weapon);
    //void addScanner (Scanner);
    void setHealth (Robot, int);
    void setTurnAngle (Robot, float);
    void setOffsetAngle (Robot, float);
    void setThrottle (Robot, float);
    float turn (float, float, float);
    float drive (float, float);
    void checks (Robot);
    ~Robot ();
        protected:
        bool isHit;
        float x;
        float y;
        float currentAngle;
};

#endif	/* ROBOT_H */
