#include "bullet.h"
#include "bullet.cpp"
#include "Robot.h"
//#include "Robot.cpp"
#include <iostream>
#include <vector>
#include <math.h>
using namespace std;



float  check_degreeT(float degree) {
	if (degree >= 0 && degree < 360) {
		return degree;
	}
	if (degree < 0) {
		return -1;
	}
	float newDegree = fmod(degree, 360);
	return newDegree;
}

float deg_to_radT(float degree) {
  float good_degree = check_degreeT(degree);
	if (good_degree > -1) {
		float a = good_degree * PI;
		float b = a/180;
		return b;
	}
	return -1;
}

int getQuadrantT(float degree) {
	float deg = check_degreeT(degree);
	if (deg >= 0 && deg < 90) {
		return 1;
	}
	if (deg >= 90 && deg <180) {
		return 2;
	}
	if (deg >= 180 && deg <270) {
		return 3;
	}
	if (deg >= 270 && deg <360) {
		return 4;
	}
	return -1;
}



//main
int main (int argc, char* args[]) {
  cout << "\nbulletTest.cpp\n";

  cout << "test Robots:\n";
  Robot robot;
  robot.x = 100;
  robot.y = 100;
  //cout << "robot location (" << robot.x << ", " << robot.y << ")\n";
  //making more roobots
  Robot aR; aR.x = 125; aR.y = 150;
  Robot bR; bR.x = 200; bR.y = 100;
  Robot cR; cR.x = 300; cR.y = 50;

  // making a vector of robots
  std::vector<Robot> testrobots(4);
  testrobots[0] = robot;
  testrobots[1] = aR;
  testrobots[2] = bR;
  testrobots[3] = cR;
  cout << "looping method 1\n";
  for (auto & r : testrobots) {
    cout << "robot location (" << r.x << ", " << r.y << ")\n";
  }
  cout << "\n";
  cout << "loopin method 2\n";
  for (int i = 0; i < testrobots.size(); i++) {
    cout << "robot location (" << testrobots.at(i).x << ", "
      << testrobots.at(i).y << ")\n";
  }


//testing bullets

  cout << "\ntest Bullets:\n";
  Bullet aB(50, 50, 90);
  cout << "bullet location: (" << aB.getX1() << ", " << aB.getY1()
    << ")\n";
  cout << "bullet direction: " << aB.getDirection() << "\n";

  //making more Bullets
  Bullet bB(25, 25, 0);
  Bullet cB(70, 75, 180);
  Bullet dB(60, 100, 270);
  Bullet eB(200, 40, 360);
  Bullet fB(50, 100, 45);
  std::vector<Bullet> bullets;
  bullets.push_back(aB);
  bullets.push_back(bB);
  bullets.push_back(cB);
  bullets.push_back(dB);
  bullets.push_back(eB);
  bullets.push_back(fB);
  cout << "\n";
  for (auto & b : bullets) {
    cout << "(" << b.getX1() << ", " << b.getY1() << ", " <<
      b.getDirection() << ") " << b.is_valid() << "\n";

  }
  cout << "good for bullets a-F\n";
  //Bullet G(-10, 100, 90);
  //Bullet H(10, -100, 90);
  Bullet I(90, 70, -40); // problems
  cout << I.getX1() << "\n";
  cout << I.getDirection() << "\n";
  cout << I.is_valid() << "\n";
  /*
  try {
    Bullet I(90, 70, -40);
    cout << I.getX1();
  } catch (...) {
    cout << "Bullet I is invalid\n";
  }
*/


  //test of check_degree
  cout << "\n" << "check_degree tests\n";
  cout << check_degreeT(0) << "\n";
  cout << check_degreeT(45) << "\n";
  cout << check_degreeT(90) << "\n";
  cout << check_degreeT(180) << "\n";
  cout << check_degreeT(270) << "\n";
  cout << check_degreeT(360) << "\n";
  cout << check_degreeT(500) << "\n";
  cout << check_degreeT(45.89) << "\n";
  cout << check_degreeT(700.458) << "\n";
  cout << "should be -1:\n";
  cout << check_degreeT(-100) << "\n";
  cout << check_degreeT(-100.98) << "\n";
  cout << check_degreeT(-1) << "\n";


  //test of deg_to_rad
  cout << "\n deg_to_rad tests\n";
  cout << deg_to_radT(0) << "\n";
  cout << deg_to_radT(45) << "\n";
  cout << deg_to_radT(90) << "\n";
  cout << deg_to_radT(180) << "\n";
  cout << deg_to_radT(270) << "\n";
  cout << deg_to_radT(360) << "\n";
  cout << deg_to_radT(500) << "\n";
  cout << deg_to_radT(45.89) << "\n";
  cout << deg_to_radT(700.458) << "\n";
  cout << "should be -1: \n";
  cout << deg_to_radT(-100) << "\n";
  cout << deg_to_radT(-100.98) << "\n";
  cout << deg_to_radT(-1) << "\n";

  //check of getQuadrant
  cout << "\n getQuadrant tests\n";
  cout << getQuadrantT(0) << "\n";
  cout << getQuadrantT(45) << "\n";
  cout << getQuadrantT(90) << "\n";
  cout << getQuadrantT(115) << "\n";
  cout << getQuadrantT(180) << "\n";
  cout << getQuadrantT(215) << "\n";
  cout << getQuadrantT(270) << "\n";
  cout << getQuadrantT(315) << "\n";
  cout << getQuadrantT(360) << "\n";
  cout << getQuadrantT(500) << "\n";
  cout << getQuadrantT(45.89) << "\n";
  cout << getQuadrantT(700.458) << "\n";
  cout << "should be -1:\n";
  cout << getQuadrantT(-100) << "\n";
  cout << getQuadrantT(-100.98) << "\n";
  cout << getQuadrantT(-1) << "\n";

  //check of collide_wall
  cout << "\n check of collode_wall\n";
  Bullet wallbad1(0,0,45);
  cout << "collides? ";
  if (wallbad1.collide_wall(25,25,640,480)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }
  Bullet wallbad2(0,-1,45);
  cout << "collides? ";
  if (wallbad2.collide_wall(25,25,640,480)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }
  Bullet wallbad3(700,500,45);
  cout << "collides? ";
  if (wallbad3.collide_wall(25,25,640,480)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }
  Bullet wallgood(50,50,90);
  cout << "collides? ";
  if (wallgood.collide_wall(25,25,640,480)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }

  //check of get_distance
  cout << "\n check of get_distance\n";
  Robot distR1; distR1.x = 100; distR1.y = 100;
  Robot distR2; distR2.x = 300; distR2.y = 75;
  Bullet distA(200,200,90);
  Bullet distB(100,100,90);
  Bullet distC(110,100,90);
  Bullet distD(305,80,90);
  cout << distA.get_distance(distR1) << "\n";
  cout << distB.get_distance(distR1) << "\n";
  cout << distC.get_distance(distR1) << "\n";
  cout << distD.get_distance(distR1) << "\n";
  cout << distA.get_distance(distR2) << "\n";
  cout << distB.get_distance(distR2) << "\n";
  cout << distC.get_distance(distR2) << "\n";
  cout << distD.get_distance(distR2) << "\n";

  //check of collide_robot
  cout << "\n check of collide_robot\n";
  Bullet hit1(100,100,90);
  Bullet hit2(98,103,45);
  Bullet hit3(130,149,90);
  Bullet hit4(201,110,270);
  Bullet hit5(307,54,180);
  Bullet miss1(300, 70,180);
  Bullet miss2(30,300,67);
  cout << "hit1, testRobots: should be t\n";
  if (hit1.collide_robot(testrobots)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }
  cout << "hit2, testRobots: should be t\n";
  if (hit2.collide_robot(testrobots)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }
  cout << "hit3, testRobots: should be t\n";
  if (hit3.collide_robot(testrobots)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }
  cout << "hit4, testRobots: should be t\n";
  if (hit4.collide_robot(testrobots)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }
  cout << "hit5, testRobots: should be t\n";
  if (hit5.collide_robot(testrobots)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }
  cout << "miss1, testRobots: should be f\n";
  if (miss1.collide_robot(testrobots)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }
  cout << "miss2, testRobots: should be f\n";
  if (miss2.collide_robot(testrobots)) {
    cout << "true\n";
  } else {
    cout << "false\n";
  }

  //motion function tests
  /*
  Bullet hit1(100,100,90);
  Bullet hit2(98,103,45);
  Bullet hit3(130,149,90);
  Bullet hit4(201,110,270);
  Bullet hit5(307,54,180);
  Bullet miss1(300, 70,180);
  Bullet miss2(30,300,67);
  */
  cout << "\n test of nextX1\n";
  cout << hit1.getX1() << " , ";
  hit1.setX1(hit1.nextX1());
  cout << hit1.getX1() << "\n";
  cout << hit5.getX1() << " , ";
  hit5.setX1(hit5.nextX1());
  cout << hit5.getX1() << "\n";

  cout << "\n test of nextY1\n";
  cout << hit1.getY1() << " , ";
  hit1.setY1(hit1.nextY1());
  cout << hit1.getY1() << "\n";
  cout << hit5.getY1() << " , ";
  hit5.setY1(hit5.nextY1());
  cout << hit5.getY1() << "\n";

  cout << "\n test of nextX2\n";
  cout << hit1.getX2() << " , ";
  hit1.setX2(hit1.nextX2());
  cout << hit1.getX2() << "\n";
  cout << hit5.getX2() << " , ";
  hit5.setX2(hit5.nextX2());
  cout << hit5.getX2() << "\n";

  cout << "\n test of nextY2\n";
  cout << hit1.getY2() << " , ";
  hit1.setY2(hit1.nextY2());
  cout << hit1.getY2() << "\n";
  cout << hit5.getY2() << " , ";
  hit5.setY2(hit5.nextY2());
  cout << hit5.getY2() << "\n";

  cout << "\n test of move()\n";
  cout << "(" << hit1.getX1() << ", " << hit1.getY1() << ") -> ";
  hit1.move();
  cout << "(" << hit1.getX1() << ", " << hit1.getY1() << ")\n";

  //test of keep_moving()
  std::vector<Bullet> inPlay;
  inPlay.push_back(hit1);
  inPlay.push_back(hit2);
  inPlay.push_back(hit3);
  inPlay.push_back(hit4);
  inPlay.push_back(hit5);
  inPlay.push_back(miss1);
  inPlay.push_back(miss2);
  cout << "\n test of keep_moving()\n";
  for (auto & b : inPlay) {
    cout << "keep_moving? ";
    if (b.keep_moving(arenaX, arenaY, arenaW, arenaH, testrobots)) {
      cout << " true\n";
    } else {
      cout << " false\n";
    }
  }


  return 0;
}