class Weapon {
public:
    Weapon();
    virtual ~Weapon() = 0;
    void rotate(int direction);
    void rotateWith(float robotTurnSpeed);
    void aim(float degree);
    virtual void draw() = 0;
    virtual bool shoot(float offset) = 0;
  
}

Weapon::Weapon(){
  
}

Weapon::~Weapon(){
  
}

void Weapon::rotate(int direction){
  
}

void Weapon::rotateWith(float robotTurnSpeed){
  
}

void Weapon::aim(float degree){
  
}
  
class Gun:private Weapon {
public:
    Gun(Robot* robot);
    ~Gun();
    void rotate(int direction);
    void rotateWith(float robotTurnSpeed);
    void aim(float degree);
    void unAim();
    void draw(SDL_Renderer* renderer);
    bool shoot(float offset);
    void tick();
private:
    float turnSpeed = 1.2;
    int fireRate = 3;
    int cooldown;
    bool canShoot;
    Robot* robot;
    float targetAngle;
    float currentAngle;
    Bullet[]* bullets;
    bool isAiming;
}

Gun::Gun(Robot* robot, Bullet[]* bullets) {
    this->robot = robot;
    this->bullets = bullets;
}

Gun::shoot() {
    bullets.add(new Bullet(robot.x-5*cos(robot.currentAngle*PI/180)+25*cos(angle*PI/180),
            robot.y+5*sin(robot.currentAngle*PI/180)+25*sin(angle*PI/180), angle))
}

Gun::draw(SDL_Renderer* renderer) {
    thickLineRGBA(renderer, robot.x-5*cos(robot.currentAngle*PI/180), 
            robot.y+5*sin(robot.currentAngle*PI/180), 
            robot.x-5*cos(robot.currentAngle*PI/180)+25*cos(angle*PI/180), 
            robot.y+5*sin(robot.currentAngle*PI/180)+25*sin(angle*PI/180), //don't forget to define PI
            4, 0x00, 0x00, 0x00, 0xff);
}

Gun::tick() {
    if(!isAiming){
        currentAngle = robot.currentAngle;
    }
    else {
        if(currentAngle != targetAngle) {
            if(abs(currentAngle - targetAngle) > turnSpeed){
                if(currentAngle < targetAngle)
                    currentAngle += turnSpeed;
                else
                    currentAngle -= turnSpeed;
            }
            else
                currentAngle = targetAngle;
        }
    } 
}

Gun::aim(float degree) {
    targetAngle = degree;
    isAiming = true;
}

Gun::unAim() {
    isAiming = false;
}